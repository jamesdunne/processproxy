// +build linux

package main

import (
	"fmt"
	"os"
	"syscall"
)

const path = "/bin/echo" // REPLACE ME
const log_path = "/tmp/proxy.log"

func log_error(line string) {
	f, err := os.OpenFile(log_path, os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0600)
	if err != nil {
		panic(err)
	}

	defer f.Close()

	if _, err = f.WriteString(line); err != nil {
		panic(err)
	}
}

func main() {
	argv := os.Args
	envv := os.Environ()

	log_error("\n----\n")
	log_error(fmt.Sprintf("argv = %v\n", argv))
	log_error(fmt.Sprintf("envv = %v\n", envv))
	log_error(fmt.Sprintf("exec(\"%s\")\n", path))

	// On Linux, exec simply replaces the current process and leaves all fds open
	err := syscall.Exec(path, argv, envv)
	if err != nil {
		log_error(fmt.Sprintf("exec(\"%s\"): %v\n", path, err))
		os.Exit(254)
		return
	}
}
